package fr.milleis.poc.rabbitmq.consumer;

import com.rabbitmq.client.Channel;
import fr.milleis.poc.rabbitmq.NotificationBean;
import fr.milleis.poc.rabbitmq.lib.consumer.ConsumerHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Random;

@Service
@Slf4j
public class ConsumerService implements ChannelAwareMessageListener {

    private final MessageConverter messageConverter;

    public ConsumerService(MessageConverter messageConverter) {
        this.messageConverter = messageConverter;
    }

    @Override
    public void onMessage(Message message, Channel channel) throws IOException {
        NotificationBean convertedMessage = (NotificationBean) messageConverter.fromMessage(message);
        log.info("Consuming the message: {}", convertedMessage.toString());

        if (manageMessageSuccess(convertedMessage)) {
            log.info("Message with id {} management exit with success", ConsumerHelper.extractMessageId(message));
            ConsumerHelper.ackMessage(message, channel);
        }
        else {
            log.info("Message with id {} management exit with error", ConsumerHelper.extractMessageId(message));
            ConsumerHelper.nackMessage(message, channel);
        }
    }

    private boolean manageMessageSuccess(NotificationBean convertedMessage) {
        return new Random().nextBoolean();
    }
}
