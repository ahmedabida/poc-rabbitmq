package fr.milleis.poc.rabbitmq.consumer;

import fr.milleis.poc.rabbitmq.lib.consumer.AbstractConsumerConfiguration;
import fr.milleis.poc.rabbitmq.properties.QueueProperties;
import org.springframework.stereotype.Component;

@Component
public class ConsumerConfiguration extends AbstractConsumerConfiguration {

    public ConsumerConfiguration(ConsumerService consumerService, QueueProperties queueProperties) {
        super(consumerService, queueProperties.getConsumersNumber(), queueProperties.getNames());
    }
}
