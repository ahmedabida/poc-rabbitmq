package fr.milleis.poc.rabbitmq.producer;

import fr.milleis.poc.rabbitmq.NotificationBean;
import fr.milleis.poc.rabbitmq.lib.producer.ProducerHelper;
import fr.milleis.poc.rabbitmq.properties.QueueProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProducerService {

    private final ProducerHelper producerHelper;
    private final QueueProperties queueProperties;

    public ProducerService(ProducerHelper producerHelper, QueueProperties queueProperties) {
        this.producerHelper = producerHelper;
        this.queueProperties = queueProperties;
    }

    public void sendMessageToExchange(NotificationBean notificatorBean) {
        log.info("sending message now");
        producerHelper.pushMessage(notificatorBean, queueProperties.getNames()[0]);
    }

}
