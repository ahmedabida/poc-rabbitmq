package fr.milleis.poc.rabbitmq.producer;

import fr.milleis.poc.rabbitmq.lib.producer.AbstractExchangeConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProducerConfiguration extends AbstractExchangeConfiguration {

    protected ProducerConfiguration() {
        super(null);
    }
}
