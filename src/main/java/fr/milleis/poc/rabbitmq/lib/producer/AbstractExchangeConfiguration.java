package fr.milleis.poc.rabbitmq.lib.producer;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

public abstract class AbstractExchangeConfiguration {

    private final String exchangeName;

    protected AbstractExchangeConfiguration(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    @Bean
    public DirectExchange mainExchange() {
        if (StringUtils.isEmpty(exchangeName)) {
            return DirectExchange.DEFAULT;
        }
        return new DirectExchange(exchangeName);
    }

}
