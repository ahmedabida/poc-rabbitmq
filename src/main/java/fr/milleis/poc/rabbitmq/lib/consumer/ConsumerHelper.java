package fr.milleis.poc.rabbitmq.lib.consumer;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.DirectMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;

import java.io.IOException;

public class ConsumerHelper {

    public static DirectMessageListenerContainer createContainerWith(ConnectionFactory connectionFactory,
                                                       MessageListenerAdapter listenerAdapter,
                                                       int consumersNumberPerQueue) {
        DirectMessageListenerContainer container = new DirectMessageListenerContainer(connectionFactory);
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.setMessageListener(listenerAdapter);
        container.setConsumersPerQueue(consumersNumberPerQueue);
        return container;
    }

    public static void ackMessage(Message message, Channel channel) throws IOException {
        channel.basicAck(extractDeliveryTagFrom(message), false);
    }

    public static void nackMessage(Message message, Channel channel) throws IOException {
        channel.basicNack(extractDeliveryTagFrom(message), false, true);
    }

    private static long extractDeliveryTagFrom(Message message) {
        final MessageProperties messageProperties = message.getMessageProperties();
        return messageProperties.getDeliveryTag();
    }

    public static String extractMessageId(Message message) {
        return message.getMessageProperties().getMessageId();
    }
}
