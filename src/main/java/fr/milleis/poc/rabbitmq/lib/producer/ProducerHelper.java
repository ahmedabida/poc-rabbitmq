package fr.milleis.poc.rabbitmq.lib.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ProducerHelper {

    private final RabbitTemplate rabbitTemplate;

    public void pushMessage(Serializable objectToSend, String routingKey, String exchangeName) {
        Message messageToSend = rabbitTemplate.getMessageConverter().toMessage(objectToSend, createMessageProperties());

        rabbitTemplate.send(exchangeName, routingKey, messageToSend);
    }

    public void pushMessage(Serializable objectToSend, String routingKey) {
        Message messageToSend = rabbitTemplate.getMessageConverter().toMessage(objectToSend, createMessageProperties());

        rabbitTemplate.send(routingKey, messageToSend);
    }

    private static String forgeMessageId() {
        return System.currentTimeMillis() + "--" + UUID.randomUUID().toString();
    }

    private static MessageProperties createMessageProperties() {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setMessageId(forgeMessageId());
        return messageProperties;
    }

}
