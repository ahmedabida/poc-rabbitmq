package fr.milleis.poc.rabbitmq.lib.consumer;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.DirectMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.context.annotation.Bean;

public abstract class AbstractConsumerConfiguration {

    private final ChannelAwareMessageListener consumerService;
    private final int consumersNumberPerQueue;
    private final String[] queueNames;

    public AbstractConsumerConfiguration(ChannelAwareMessageListener consumerService, int consumersNumberPerQueue, String[] queueNames) {
        this.consumerService = consumerService;
        this.consumersNumberPerQueue = consumersNumberPerQueue;
        this.queueNames = queueNames;
    }

    @Bean
    public MessageListenerContainer listenerContainer(ConnectionFactory connectionFactory,
                                                      MessageListenerAdapter emailListenerAdapter) {
        DirectMessageListenerContainer container = ConsumerHelper.createContainerWith(connectionFactory, emailListenerAdapter, consumersNumberPerQueue);
        container.setQueueNames(queueNames);
        return container;
    }

    @Bean
    public MessageListenerAdapter emailListenerAdapter() {
        return new MessageListenerAdapter(consumerService);
    }
}
