package fr.milleis.poc.rabbitmq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationBean implements Serializable {

    private String firstName;
    private String lastName;
    private Instant dateOfBirth;

}
