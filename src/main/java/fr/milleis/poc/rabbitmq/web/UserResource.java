package fr.milleis.poc.rabbitmq.web;

import fr.milleis.poc.rabbitmq.NotificationBean;
import fr.milleis.poc.rabbitmq.producer.ProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserResource {

    private final ProducerService producerService;

    @PostMapping("/create")
    public ResponseEntity<NotificationBean> sendSingleMessage(@RequestBody NotificationBean message) {
        producerService.sendMessageToExchange(message);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
