package fr.milleis.poc.rabbitmq.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("queues.properties")
public class QueueProperties {

    private String[] names;
    private int consumersNumber;

}
