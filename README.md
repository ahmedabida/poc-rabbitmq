# Goal
The goal for this repository is to introduce RabbitMQ, as a Messaging Broker for Milleis projects  

# RabbitMQ

## Deployment
RabbitMQ is deployed on a Docker container, accessible [here](http://10.101.147.111:9595/#/).

In Sit, RabbiMQ is deployed [here](http://rbmq-dl-asr01:15672/#/).

## More about AMQP, the protocol used by RabbitMQ
https://spring.io/blog/2010/06/14/understanding-amqp-the-protocol-used-by-rabbitmq

